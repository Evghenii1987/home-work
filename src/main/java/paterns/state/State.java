    package paterns.state;

    public class State {
        public static void main(String[] args) {
            StateI open= new Open();
            Door door= new Door();
            door.setStateI(open);

            for (int i = 0; i <5 ; i++) {
                door.moveIt();
                door.chancgeState();
            }
        }
    }

    class Door {
        private StateI stateI;

        void setStateI(StateI stateI) {
            this.stateI = stateI;
        }

        void chancgeState() {
            if (stateI instanceof Open) {
                setStateI(new Close());
            } else if (stateI instanceof Close) {
                setStateI(new Open());
            }
        }
        void moveIt(){
            stateI.openClose();
        }
    }

    interface StateI {
        void openClose();
    }

    class Open implements StateI {

        @Override
        public void openClose() {
            System.out.println("Door is open!");
        }
    }

    class Close implements StateI {

        @Override
        public void openClose() {
            System.out.println("Door is close!");
        }
    }