    package paterns.adapter;

    import java.util.ArrayList;

    public class AdapterP {
//        We need it for example if we have something like this
        ArrayList<PrintI>printIS=new ArrayList<>();

        public void printIS(){
            for (PrintI p: printIS) {
                p.consolePrint();
            }
        }
    }

    class Client {
        public static void main(String[] args) {
    //        Version 1
    //        PrintI print = new PrintSomething();
    //        print.consolePrint();
    //        Version 2.0
    //        PrintI printcomp = new PrintComposition(new Printer());
    //        Version 2.1
            PrintI printcomp = new PrintComposition();
            printcomp.consolePrint();
        }
    }

    interface PrintI {
        void consolePrint();
    }

    //Version  1
    class PrintSomething extends Printer implements PrintI {
        @Override
        public void consolePrint() {
            print();
        }
    }

    //Version 2
    class PrintComposition implements PrintI {
    //    Version 2.0
    //    private final Printer printer;
    //
    //    PrintComposition(Printer printer) {
    //        this.printer = printer;
    //    }

        //    Version 2.1
        private Printer printer = new Printer();

        @Override
        public void consolePrint() {
            printer.print();
        }
    }

    class Printer {
        void print() {
            System.out.println("Hello Someone!");
        }
    }