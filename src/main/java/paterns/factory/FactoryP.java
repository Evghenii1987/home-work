package paterns.factory;

import java.util.ArrayList;
import java.util.List;

public class FactoryP {
    public static void main(String[] args) {
        List<Chocolate> chocolates= new ArrayList<>();

        Factory factory=new CreatorChocolate(1);
        Chocolate chocolate= factory.create();
        chocolate.getPrise();

    }

}

/**
 *
 */
interface Chocolate{
    void getPrise();
}

class WhiteChocolate implements Chocolate{
    private final int price=15;

    @Override
    public void getPrise() {
        System.out.println(price);
    }
}

class BlackChocolate implements Chocolate{
    private final int price=10;

    @Override
    public void getPrise() {
        System.out.println(price);
    }
}

abstract class Factory{
       int flag;

    Factory(int flag){
        this.flag=flag;
    }

    abstract Chocolate create();
}

class CreatorChocolate extends Factory{

    CreatorChocolate(int flag) {
        super(flag);
    }

    @Override
    Chocolate create() {
     if (flag==0)
        return  new WhiteChocolate();

     if (flag==1)
         return new BlackChocolate();

     throw new IllegalArgumentException();
    }
}