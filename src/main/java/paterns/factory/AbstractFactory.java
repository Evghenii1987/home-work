package paterns.factory;

public class AbstractFactory {

    public static void main(String[] args) {
        FactoryAbstract factoryAbstract= new FactoryChocolate();
        factoryAbstract.getCake().name();
        factoryAbstract.getCake().prise();
        factoryAbstract.getCocktail().name();
        factoryAbstract.getCocktail().price();
        System.out.println("-----------------------------------");
       FactoryAbstract factoryAbstract1= new FactoryCream();
       factoryAbstract1.getCake().name();
       factoryAbstract1.getCake().prise();
       factoryAbstract1.getCocktail().name();
       factoryAbstract1.getCocktail().price();
    }
}

interface FactoryAbstract {
    Cake getCake();

    Cocktails getCocktail();
}

class FactoryChocolate implements FactoryAbstract {

    @Override
    public Cake getCake() {
        return new ChocolateCake();
    }

    @Override
    public Cocktails getCocktail() {
        return new ChocolateCocktail();
    }
}

class FactoryCream implements FactoryAbstract {

    @Override
    public Cake getCake() {
        return new CreamCake();
    }

    @Override
    public Cocktails getCocktail() {
        return new CreamCocktail();
    }
}

/**
 *
 */
interface Cake {
    void name();

    void prise();
}

class ChocolateCake implements Cake {

    @Override
    public void name() {
        System.out.println("Chocolate Cake");
    }

    @Override
    public void prise() {
        System.out.println(60);
    }
}

class CreamCake implements Cake {

    @Override
    public void name() {
        System.out.println("Cream cake");
    }

    @Override
    public void prise() {
        System.out.println(50);
    }
}

interface Cocktails {
    void name();

    void price();
}

class ChocolateCocktail implements Cocktails {

    @Override
    public void name() {
        System.out.println("Chocolate Cocktail");
    }

    @Override
    public void price() {
        System.out.println(15);
    }
}

class CreamCocktail implements Cocktails {

    @Override
    public void name() {
        System.out.println("Cream Cocktail");
    }

    @Override
    public void price() {
        System.out.println(12);
    }
}